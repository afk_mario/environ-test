import os

import environ

ROOT_DIR = environ.Path(__file__) - 1

env = environ.Env()

env.read_env(str(ROOT_DIR.path('.env')))
print(str(ROOT_DIR.path('.env')))

TEST = env('TEST')
TEST_OS = os.environ.get('TEST')

print(f"TEST: {TEST}")
print(f"TEST_OS: {TEST_OS}")
